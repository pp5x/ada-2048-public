-- A1: Random Generator Interface
package Random is
  procedure Init(Seed : Integer := 42);
  function Get_Next return Integer;
end Random;
