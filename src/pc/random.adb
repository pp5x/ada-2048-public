with Interfaces; use Interfaces;
with Ada.Unchecked_Conversion;

-- A1: Software Random Generator used for i386 version.
package body Random is
  m_w : Unsigned_32 := 37;  --  /* must not be zero, nor 0x464fffff */
  m_z : Unsigned_32 := 464;   -- /* must not be zero, nor 0x9068ffff */

  function To_Integer is new Ada.Unchecked_Conversion (Unsigned_32, Integer);
  procedure Init (Seed : Integer := 42) is
    dummy : Integer;
    pragma Unreferenced (dummy);
  begin
    m_z := Unsigned_32(seed);
    m_w := Unsigned_32(seed);
    dummy := Get_Next;
  end init;

  function Get_Next return Integer is
  begin
    m_z := 36969 * (m_w and 65535) + (Shift_Right(m_z, 16));
    m_w := 18000 * (m_z and 65535) + (Shift_Right(m_w, 16));
    return To_Integer (Shift_Left(m_z, 16) + m_w); -- /* 32-bit result */ end Get_Next;
  end Get_Next;
end Random;
