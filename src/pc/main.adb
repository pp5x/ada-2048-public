with Ada.Text_IO; use Ada.Text_IO;
with Game_Grid;
with Tile;
with Random;

procedure Main is
  c : Character;
  m : Game_Grid.Matrix;
  Win : Boolean := False;
begin
  m := Game_Grid.Init;
  Random.Init;

  -- Display part
  Put_Line("------------------------------------------------------------");
  Put_Line("|   Press w, a, s, or d and Enter to move in a direction   |");
  Put_Line("------------------------------------------------------------");
  Put_Line("");
  for i in Game_Grid.Height_Range loop
    Put_Line("--------------------------------");
    Put_Line("|       |       |       |       |");
    for j in Game_Grid.Width_Range loop
      if Tile.Get_Value(m(i, j)) > 999 then
        Put("| ");
      elsif Tile.Get_Value(m(i, j)) > 99 then
        Put("|  ");
      elsif Tile.Get_Value(m(i, j)) > 9 then
        Put("|   ");
      else
        Put("|    ");
      end if;
      if Tile.Get_Value(m(i, j)) = 0 then
        Put("  ");
      else
        Put(Integer'Image(Tile.Get_Value(m(i, j))));
      end if;
      Put(" ");
    end loop;
    Put_line("|");
    Put_Line("|       |       |       |       |");
  end loop;
  Put_Line("--------------------------------");
  -- End display part

  Main_Loop :
  loop
    exit Main_Loop when Game_Grid.is_Game_Over(m);
    Get(c);
    exit Main_Loop when c = 'e';
    if c = 'q' then
      Game_Grid.Update(m, Game_Grid.Left);
    elsif c = 's' then
      Game_Grid.Update(m, Game_Grid.Down);
    elsif c = 'd' then
      Game_Grid.Update(m, Game_Grid.Right);
    elsif c = 'w' then
      Game_Grid.Update(m, Game_Grid.Up);
    end if;
    -- Display part
    Put_Line("------------------------------------------------------------");
    Put_Line("|   Press w, a, s, or d and Enter to move in a direction   |");
    Put_Line("------------------------------------------------------------");
    Put_Line("");
    for i in Game_Grid.Height_Range loop
      Put_Line("--------------------------------");
      Put_Line("|       |       |       |       |");
      for j in Game_Grid.Width_Range loop

        if Tile.Get_Value(m(i, j)) > 999 then
          Put("| ");
        elsif Tile.Get_Value(m(i, j)) > 99 then
          Put("|  ");
        elsif Tile.Get_Value(m(i, j)) > 9 then
          Put("|   ");
        else
          Put("|    ");
        end if;
        if Tile.Get_Value(m(i, j)) = 0 then
          Put("  ");
        else
          Put(Integer'Image(Tile.Get_Value(m(i, j))));
        end if;
        Put(" ");
      end loop;
      Put_line("|");
      Put_Line("|       |       |       |       |");
    end loop;
    Put_Line("--------------------------------");
    -- End display part

  end loop Main_Loop;

  for i in Game_Grid.Height_Range loop
    for j in Game_Grid.Width_Range loop
      if Tile.Get_Value(m(i, j)) = 2048 then
        Win := True;
        exit;
      end if;
    end loop;
  end loop;

  Put_Line("---------------------------");
  if Win then
    Put_Line("|           Win           |");
  else
    Put_Line("|        Game Over        |");
  end if;
  Put_Line("---------------------------");
end Main;
