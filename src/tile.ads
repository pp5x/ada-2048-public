package Tile is

  -- LLR-HLR2-1: Defines tile values (powers of 2 up to 2048).
  subtype Tile_Value is Integer
  with Dynamic_Predicate => Tile_Value in 0 | 2 | 4 | 8 | 16 | 32 | 64 | 128 | 256 | 512 | 1024 | 2048;
  type Tile_Type is private;

  -- LLR-HLR3-1: Randomly initialize a tile (value 2 or 4).
  function Init(B : Boolean) return Tile_Type
    with Post => (if B then
                  (Get_Value(Init'Result) = 2 or Get_Value(Init'Result) = 4) and not Is_Merged(Init'Result)
                  else Get_Value(Init'Result) = 0 and not Is_Merged(Init'Result));
  
  function Get_Value(Tile : Tile_Type) return Integer;

  procedure Set_Value(Tile : in out Tile_Type; Value : Integer)
    with Post => Get_Value(Tile) = Value;

  Procedure Move_Tile(S_Tile : in out Tile_Type; E_Tile : in out Tile_Type)
    with Pre => (Get_Value(E_Tile) = 0
                 and Get_Value(S_Tile) /= 0),
         Post => (Get_Value(S_Tile) = 0 and not Is_Merged(S_Tile)
                  and Get_Value(E_Tile) = Get_Value(S_Tile'Old) and not Is_Merged(E_Tile));

  -- LLR-HLR5-2: Compute the merge between two compatible tiles.
  Procedure Merge_Tiles(S_Tile : in out Tile_Type; E_Tile : in out Tile_Type)
    with Pre => (Get_Value(S_Tile) /= 0
                  and Get_Value(S_Tile) = Get_Value(E_Tile)
                  and not Is_Merged(S_Tile)
                  and not Is_Merged(E_Tile)),
         Post => (Get_Value(S_Tile) = 0
                  and Get_Value(E_Tile) = Get_Value(E_Tile'Old) * 2
                  and Is_Merged(E_Tile)
                  and not Is_Merged(S_Tile));

  function Is_Merged(Tile : Tile_Type) return Boolean;

  procedure Set_Merged(Tile : in out Tile_Type; Merged : Boolean)
    with Post => Is_Merged(Tile) = Merged;

  private

  procedure Inc_Value(Tile : in out Tile_Type)
    with Pre => Get_Value(Tile) /= 0,
         Post => Get_Value(Tile) = Get_Value(Tile'Old) * 2;

  procedure Reset_Value(Tile : in out Tile_Type)
    with Post => Get_Value(Tile) = 0;

  -- LLR-HLR2-1: Private type of a tile.
  type Tile_Type is
    record
      Value   : Tile_Value;
      Merged : Boolean;
    end record;
end Tile;
