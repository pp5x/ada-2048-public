with STM32F4; use STM32F4;
with STM32F4.Reset_Clock_Control;
with System;

-- A1: Hardware Random Generator used for arm version.
package body Random is
  --  Random number generator enable
  type Pin_CR_RNGEN is
    (Disable, Enable);

  for Pin_CR_RNGEN use
    (Disable    => 0,
     Enable     => 1);
  for Pin_CR_RNGEN'Size use 1;

  --  Interrupt Enable
  type Pin_CR_IE is
    (Disable, Enable);

  for Pin_CR_IE use
    (Disable    => 0,
     Enable     => 1);
  for Pin_CR_IE'Size use 1;

  --  RNG control register
  type RNG_CR is limited record
    RNGEN       : Pin_CR_RNGEN;
    IE          : Pin_CR_IE;
  end record with Size => Word'Size, Volatile_Full_Access;

  for RNG_CR use record
    RNGEN       at 0 range 2 .. 2;
    IE          at 0 range 3 .. 3;
  end record;


  --  Data Ready
  type Pin_SR_DRDY is
    (NotReady, Ready);

  for Pin_SR_DRDY use
    (NotReady   => 0,
     Ready      => 1);
  for Pin_SR_DRDY'Size use 1;

  --  RNG status register
  type RNG_SR is limited record
    DRDY        : Pin_SR_DRDY;
  end record with Size => Word'Size, Volatile_Full_Access;

  for RNG_SR use record
    DRDY        at 0 range 0 .. 0;
  end record;


  -- RNG data register
  --  type RNG_DR is new Word;

  --  RNG registers
  type RNG is limited record
    CR : RNG_CR;
    SR : RNG_SR;
    DR : Integer;
  end record with Size => Word'Size * 3;

  RNG_Regs : RNG with Volatile, Import,
                      Address => System'To_Address (16#5006_0800#);

  procedure Init (Seed : Integer := 42) is
    pragma Unreferenced (Seed);
  begin
    STM32F4.Reset_Clock_Control.RNG_Force_Reset;
    STM32F4.Reset_Clock_Control.RNG_Clock_Enable;
    STM32F4.Reset_Clock_Control.RNG_Release_Reset;

    RNG_Regs.CR.RNGEN := Enable;
    RNG_Regs.CR.IE := Disable;
  end Init;

  function Get_Next return Integer is
  begin
    loop
      exit when RNG_Regs.SR.DRDY = Ready;
    end loop;
    return RNG_Regs.DR;
  end Get_Next;
end Random;
