package body ScreenFonts is
  subtype CharFontWidth is Natural range 0 .. CHAR_FONT_WIDTH - 1;
  subtype CharFontHeight is Natural range 0 .. CHAR_FONT_HEIGHT - 1;

  type CharFont is
    array (CharFontHeight,
           CharFontWidth) of Natural range 0 .. 1;

  Fonts : constant array (Character range '0' .. '9') of CharFont :=
    ('0' => ((0, 0, 1, 1, 1, 0, 0),
             (0, 1, 0, 0, 1, 1, 0),
             (1, 1, 0, 0, 0, 1, 1),
             (1, 1, 0, 0, 0, 1, 1),
             (0, 1, 1, 0, 0, 1, 0),
             (0, 0, 1, 1, 1, 0, 0)),

     '1' => ((0, 0, 0, 1, 1, 0, 0),
             (0, 0, 1, 1, 1, 0, 0),
             (0, 0, 0, 1, 1, 0, 0),
             (0, 0, 0, 1, 1, 0, 0),
             (0, 0, 0, 1, 1, 0, 0),
             (0, 1, 1, 1, 1, 1, 1)),

     '2' => ((0, 1, 1, 1, 1, 1, 0),
             (1, 1, 0, 0, 0, 1, 1),
             (0, 0, 0, 0, 1, 1, 1),
             (0, 1, 1, 1, 1, 0, 0),
             (1, 1, 1, 0, 0, 0, 0),
             (1, 1, 1, 1, 1, 1, 1)),

     '3' => ((0, 1, 1, 1, 1, 1, 1),
             (0, 0, 0, 0, 1, 1, 0),
             (0, 0, 1, 1, 1, 0, 0),
             (0, 0, 0, 0, 0, 1, 1),
             (1, 1, 0, 0, 0, 1, 1),
             (0, 1, 1, 1, 1, 1, 0)),

     '4' => ((0, 0, 0, 1, 1, 1, 0),
             (0, 0, 1, 1, 1, 1, 0),
             (0, 1, 1, 0, 1, 1, 0),
             (1, 1, 0, 0, 1, 1, 0),
             (1, 1, 1, 1, 1, 1, 1),
             (0, 0, 0, 0, 1, 1, 0)),

     '5' => ((1, 1, 1, 1, 1, 1, 0),
             (1, 1, 0, 0, 0, 0, 0),
             (1, 1, 1, 1, 1, 1, 0),
             (0, 0, 0, 0, 0, 1, 1),
             (1, 1, 0, 0, 0, 1, 1),
             (0, 1, 1, 1, 1, 1, 0)),

     '6' => ((0, 1, 1, 1, 1, 1, 0),
             (1, 1, 0, 0, 0, 0, 0),
             (1, 1, 1, 1, 1, 1, 0),
             (1, 1, 0, 0, 0, 1, 1),
             (1, 1, 0, 0, 0, 1, 1),
             (0, 1, 1, 1, 1, 1, 0)),

     '7' => ((1, 1, 1, 1, 1, 1, 1),
             (1, 1, 0, 0, 0, 1, 1),
             (0, 0, 0, 0, 1, 1, 0),
             (0, 0, 0, 1, 1, 0, 0),
             (0, 0, 1, 1, 0, 0, 0),
             (0, 0, 1, 1, 0, 0, 0)),

     '8' => ((0, 1, 1, 1, 1, 1, 0),
             (1, 1, 0, 0, 0, 1, 1),
             (0, 1, 1, 1, 1, 1, 0),
             (1, 1, 0, 0, 0, 1, 1),
             (1, 1, 0, 0, 0, 1, 1),
             (0, 1, 1, 1, 1, 1, 0)),

     '9' => ((0, 1, 1, 1, 1, 1, 0),
             (1, 1, 0, 0, 0, 1, 1),
             (1, 1, 0, 0, 0, 1, 1),
             (0, 1, 1, 1, 1, 1, 1),
             (0, 0, 0, 0, 0, 1, 1),
             (0, 1, 1, 1, 1, 1, 0)));

  procedure Draw_Char (C : CharFont; P : Point; RGB : Color) is
    FontP : Point;
  begin
    for J in CharFontWidth loop
      for I in CharFontHeight loop
        if C (I, J) = 1 then
          FontP := (J, I);
          Set_Pixel (FontP + P, RGB);
        end if;
      end loop;
    end loop;
  end Draw_Char;

  procedure Draw_Str (S : String; P : Point; RGB : Color) is
    CurPos : Point := P;
  begin
    for C of S loop
      if C in '0' .. '9' then
        Draw_Char (Fonts (C), CurPos, RGB);
        CurPos := CurPos + (CHAR_FONT_WIDTH + 1, 0);
      else
        CurPos := CurPos + (CHAR_FONT_WIDTH, 0);
      end if;
    end loop;
  end Draw_Str;

  function Draw_Str_Size (S : String) return Natural is
  begin
    return S'Length * CHAR_FONT_WIDTH + (S'length - 1);
  end Draw_Str_Size;

end ScreenFonts;
