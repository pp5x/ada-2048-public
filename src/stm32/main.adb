with screen_interface; use screen_interface;
with Render; use Render;
with Game_Grid;
with Random;

procedure Main is
  function GetDirection return Game_Grid.Dir is
    State : Touch_State;
    Last_X : Width := (Width'Last - Width'First) / 2;
    Last_Y : Height := (Height'Last - Height'First) / 2;
    First_X : Width := Last_X;
    First_Y : Height := Last_Y;
  begin
    -- Wait user input
    loop
      State := Get_Touch_State;
      exit when State.Touch_Detected
      and then (State.X /= Last_X or State.Y /= Last_Y);
    end loop;

    --  Draw cross.
    First_X := State.X;
    First_Y := State.Y;
    Last_X := State.X;
    Last_Y := State.Y;

    while State.Touch_Detected loop
      Last_X := State.X;
      Last_Y := State.Y;
      State := Get_Touch_State;
    end loop;

    if Last_Y - First_Y > 175 then
      return Game_Grid.Down;
    elsif Last_Y - First_Y < -175 then
      return Game_Grid.Up;
    elsif Last_X - First_X < -75 then
      return Game_Grid.Left;
    elsif Last_X - First_X > 75 then
      return Game_Grid.Right;
    end if;

    return Game_Grid.None;
  end GetDirection;
begin
  -- Screen and Random generator initialization
  Random.Init;
  Screen_Interface.Initialize;
  Screen_Interface.Fill_Screen(Black);

  declare
    M : Game_Grid.Matrix := Game_Grid.Init;
    Direction : Game_Grid.Dir := Game_Grid.None;
  begin
    -- Game loop
    loop
      Damier (Width'Last / 4, M);
      Direction := GetDirection;
      Game_Grid.Update(M, Direction);
      -- LLR-HLR7-3: Exit the game loop when the game is over.
      exit when Game_Grid.is_Game_Over(M);
    end loop;

    -- LLR-HLR8-1 / LLR-HLR8-2: Setup gameover screen to notify the player.
    declare
      Win : constant Boolean := Game_Grid.Has_Won(M);
      ScreenColor : Color;
    begin
      if Win then
        ScreenColor := Blue;
      else
        ScreenColor := Red;
      end if;

      -- Win/Gameover screen
      loop
        Screen_Interface.Fill_Screen(ScreenColor);
      end loop;
    end;
  end;
end Main;
