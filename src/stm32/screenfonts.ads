with screen_interface; use screen_interface;

package ScreenFonts is
  CHAR_FONT_WIDTH : constant Natural := 7;
  CHAR_FONT_HEIGHT : constant Natural := 6;

  procedure Draw_Str (S : String; P : Point; RGB : Color);
  function Draw_Str_Size (S : String) return Natural;
end ScreenFonts;
