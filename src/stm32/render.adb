with screen_interface; use screen_Interface;
with ScreenFonts; use ScreenFonts;
with Tile;

package body Render is
  procedure Draw_Rectangle (P : Point; W : Width; H : Height; C : Color)
  is
    PEnd : constant Point := (P.X + W - 1, P.Y + H - 1);
  begin
    for Y in P.Y .. PEnd.Y loop
      for X in P.X .. PEnd.X loop
        Set_Pixel ((X, Y), C);
      end loop;
    end loop;
  end Draw_Rectangle;

  procedure Damier (Size : Integer; M : Game_Grid.Matrix) is
  begin
    for I in Game_Grid.Width_Range loop
      for J in Game_Grid.Height_Range loop
        declare
          StrNum : constant String := Integer'Image (Tile.Get_Value(M(J, I)));
          CXSize : constant Natural := Draw_Str_Size (StrNum);
        begin
          if (I + J) mod 2 = 0 then
            Draw_Rectangle ((I * Size, J * Size), Size , Size, Gray);
          else
            Draw_Rectangle ((I * Size, J * Size), Size, Size, White);
          end if;
          if Tile.Get_Value(M(J, I)) /= 0 then
            Draw_Str (StrNum,
              (I * Size + (Size - CXSize) / 2, J * Size + (Size - CHAR_FONT_HEIGHT) / 2),
              Black);
          end if;
        end;
      end loop;
    end loop;
  end Damier;
end Render;
