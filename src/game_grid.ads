with Tile; use Tile;

-- A3: Grid manager
package Game_Grid is
  -- LLR-HLR1-1: Grid width/height with their associated subtypes.
  Width : constant Natural := 4;
  Height : constant Natural := 4;

  subtype Width_Range is Natural range  0 .. Width - 1;
  subtype Height_Range is Natural range  0 .. Height - 1;

  -- LLR-HLR4-1: User input (given direction from input device).
  type Dir is (Up, Down, Left, Right, None);

  -- LLR-HLR1-2: 2D array Matrix type using Width/Height subtypes.
  type Matrix is array (Height_Range, Width_Range) of Tile.Tile_Type;

  -- LLR-HLR1-3: Setup the initial state of the grid by
  -- randomly adding a tile (value 2 or 4).
  function Init return Matrix;

  procedure Update (M : in out Matrix; Direction : Dir);
  -- LLR-HLR7-3: No more moves are available.
  function is_Game_Over(M : Matrix) return Boolean;
  -- LLR-HLR7-4: Checks if a tile has the 2048 value: the player has won.
  function Has_Won (M : Matrix) return Boolean;

end Game_Grid;
