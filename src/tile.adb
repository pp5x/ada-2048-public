with Random;

package body Tile is
  function Init (B : Boolean)
    return Tile_Type is

    Value : Integer;
    Ret : Tile_Type;
  begin
    if B then
      Value := Integer(Random.Get_Next mod 10);

      if Value >= 8 then
        Ret := (4, False);
      else
        Ret := (2, False);
      end if;
      return Ret;
    else
      Ret := (0, False);
      return Ret;
    end if;
  end Init;

  procedure Move_Tile(S_Tile : in out Tile_Type; E_Tile : in out Tile_Type) is
  begin
    E_Tile.Value := S_Tile.Value;
    Reset_Value(S_Tile);
  end Move_Tile;

  procedure Merge_Tiles(S_Tile : in out Tile_Type; E_Tile : in out Tile_Type) is
  begin
    Inc_Value(E_Tile);
    Set_Merged(E_Tile, True);
    Reset_Value(S_Tile);
  end Merge_Tiles;

  function Get_Value(Tile : Tile_Type) return Integer is
  begin
    return Tile.Value;
  end Get_Value;

  procedure Set_Value(Tile : in out Tile_Type; Value : Integer) is
  begin
    Tile.Value := Value;
  end Set_Value;

  procedure Inc_Value(Tile : in out Tile_Type) is
  begin
    Tile.Value := Tile.Value * 2;
  end Inc_Value;

  procedure Reset_Value(Tile : in out Tile_Type) is
  begin
    Tile.Value := 0;
  end Reset_Value;

  function Is_Merged(Tile : Tile_Type) return Boolean is
  begin
    return Tile.Merged;
  end Is_Merged;

  procedure Set_Merged(Tile : in out Tile_Type; Merged : Boolean) is
  begin
    Tile.Merged := Merged;
  end Set_Merged;

end Tile;
