with Random;

package body Game_Grid is
  function Refresh_Matrix (M : in out Matrix; Direction : Dir) return Boolean;
  function Hor_Move_Tile (M : in out Matrix; Direction : Dir; PosX : in out Width_Range; PosY : in Height_Range) return Boolean;
  function Ver_Move_Tile (M : in out Matrix; Direction : Dir; PosX : in Width_Range; PosY : in out Height_Range) return Boolean;
  function check_Neighbours(M : Matrix;  PosX : Width_Range; PosY : Height_Range) return Boolean;

  function Init return Matrix is
    M : Matrix;
    Pos : Integer;
  begin
    Pos := Random.Get_Next mod 16;
    
    for Y in Game_Grid.Height_Range loop
      for X in Game_Grid.Width_Range loop
        if ((Pos mod 4 = X) and (Pos / 4 = Y)) then
          M(Y, X) := Init(True);
        else
          M(Y, X) := Tile.Init(False);
        end if;
      end loop;
    end loop;
    return M;
  end Init;

  procedure Update (M : in out Matrix; Direction : Dir) is

    Count : Integer := 0;
    Pos : Integer;
    can_move: Boolean := False;

  begin
    if Direction = None then
      return;
    end if;

    can_move := Refresh_Matrix(M, Direction);
    if can_move then

      for Y in Game_Grid.Height_Range loop
        for X in Game_Grid.Width_Range loop
          if Get_Value(M(Y, X)) = 0 then
            Count := Count + 1;
          end if;
        end loop;
      end loop;

      if Count > 0 then
        Pos := Random.Get_Next mod Count;
        Count := 0;

        for Y in Game_Grid.Height_Range loop
          for X in Game_Grid.Width_Range loop
            if Get_Value(M(Y, X)) = 0 then
              if Count = Pos then
                M(Y, X) := Tile.Init(True);
                Count := Count + 1;
              end if;
              Count := Count + 1;
            end if;
          end loop;
        end loop;
      end if;
    end if;
  end Update;

  function is_Game_Over(M : Matrix) return Boolean is
    End_Game : Boolean := True;
  begin
    for Y in Game_Grid.Height_Range loop
      for X in Game_Grid.Width_Range loop
        if check_Neighbours(M, X, Y) then
          End_Game := False;
        end if;
        if Get_Value(M(Y, X)) = 2048 then
          return True;
        end if;
      end loop;
    end loop;

    return End_Game;
  end is_Game_Over;

  function Has_Won (M : Matrix) return Boolean is
  begin
    for I in Game_Grid.Height_Range loop
      for J in Game_Grid.Width_Range loop
        if Tile.Get_Value(M(I, J)) = 2048 then
          return True;
        end if;
      end loop;
    end loop;

    return False;
  end Has_Won;

  function Refresh_Matrix(M : in out Matrix; Direction : Dir) return Boolean is

    begX : Integer := 0;
    begY : Integer := 0;
    endX : Integer := 3;
    endY : Integer := 3;
    dirX : Integer := 1;
    dirY : Integer := 1; 
    ret : Boolean := False;
  begin
    if Direction = Right then
      dirX := -1;
      begX := 3;
      endX := 0;
    elsif Direction = Down then
      dirY := -1;
      begY := 3;
      endY := 0;
    end if;
    while begY /= endY + dirY loop
      while begX /= endX + dirX loop
        if Get_Value(M(begY, begX)) /= 0 then
          if Direction = Left or Direction = Right then
            if Hor_Move_Tile (M, Direction, begX, begY) then
              ret := True;
            end if;
          else
            if Ver_Move_Tile (M, Direction, begX, begY) then
              ret := True;
            end if;
          end if;
        end if;
        begX := begX + dirX;
      end loop;
      begY := begY + dirY;
      if Direction = Right then
        begX := 3;
      else
        begX := 0;
      end if;
    end loop;
    for Y in Height_Range loop
      for X in Width_Range loop
        Set_Merged(M(Y, X), False);
      end loop;
    end loop;
    return ret;
  end Refresh_Matrix;

  -- LLR-HLR4-2: Horizontally moves tiles on the grid and merges if possible.
  -- LLR-HLR5-1: Detect possible merges.
  function Hor_Move_Tile (M : in out Matrix; Direction : Dir; PosX : in out Width_Range; PosY : in Height_Range) return Boolean is
    begX : Integer := Integer(PosX);
    endX : Integer := 3;
    dirX : Integer := 1;
    ret : Boolean := False;
  begin
    if Direction = Left then
      dirX := -1;
      endX := 0;
    end if;
    begX := begX + dirX;
    while begX /= endX+ dirX loop
      if Get_Value(M(PosY, begX)) = Get_Value(M(PosY, PosX)) and not Is_Merged(M(PosY, begX)) and not Is_Merged(M(PosY, PosX)) then
        Merge_Tiles(M(PosY, PosX), M(PosY, begX));
        PosX := begX;
        ret := True;
      end if;
      exit when Get_Value(M(PosY, begX)) /= 0 or Is_Merged(M(PosY, PosX));
      Move_Tile(M(PosY, PosX), M(PosY, begX));
      PosX := begX;
      begX := begX + dirX;
      ret := True;

    end loop;
    return ret;
  end Hor_Move_Tile;

  -- LLR-HLR4-2: Vertically moves tiles on the grid and merges if possible.
  -- LLR-HLR5-1: Detect possible merges.
  function Ver_Move_Tile (M : in out Matrix; Direction : Dir;  PosX : in Width_Range; PosY : in out Height_Range) return Boolean is

    begY : Integer := Integer(PosY);
    endY : Integer := 3;
    dirY : Integer := 1;
    ret : Boolean := False;

  begin
    if Direction = Up then
      dirY := -1;
      endY := 0;
    end if;
    begY := begY + dirY;
    while begY /= endY + dirY loop
      if Get_Value(M(begY, PosX)) = Get_Value(M(PosY, PosX)) and not Is_Merged(M(begY, PosX)) and not is_Merged(M(PosY, PosX)) then
        Merge_Tiles(M(PosY, PosX), M(begY, PosX));
        PosY := begY;
        ret := True;
      end if;
      exit when Get_Value(M(begY, PosX)) /= 0 or is_Merged(M(PosY, PosX));
      Move_Tile(M(PosY, PosX), M(begY, PosX));
      PosY := begY;
      begY := begY + dirY;
      ret := True;

    end loop;
    return ret;
  end Ver_Move_Tile;

  -- LLR-HLR6-1: Checks if a tile can move on the grid (empty or merge case), otherwise
  -- returns a boolean (True) if the tile can move.
  -- LLR-HLR7-2: Checks if two neighbouring tiles have the same value.
  function check_Neighbours(M : Matrix;  PosX : Width_Range; PosY : Height_Range)
    return Boolean is
  begin
    if PosX > Width_Range'First then
      if Get_Value(M(PosY, PosX - 1)) = 0 or Get_Value(M(PosY, PosX - 1)) = Get_Value(M(PosY, PosX)) then
        return True;
      end if;
    end if;
    if PosX < Width_Range'Last then
      if Get_Value(M(PosY, PosX + 1)) = 0 or Get_Value(M(PosY, PosX + 1)) = Get_Value(M(PosY, PosX)) then
        return True;
      end if;
    end if;
    if PosY > Height_Range'First then
      if Get_Value(M(PosY - 1, PosX)) = 0 or Get_Value(M(PosY - 1, PosX)) = Get_Value(M(PosY, PosX)) then
        return True;
      end if;
    end if;
    if PosY < Height_Range'Last then
      if Get_Value(M(PosY + 1, PosX)) = 0 or Get_Value(M(PosY + 1, PosX)) = Get_Value(M(PosY, PosX)) then
        return True;
      end if;
    end if;

    return False;
  end check_Neighbours;
end Game_Grid;
