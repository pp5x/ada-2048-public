--  This package has been generated automatically by GNATtest.
--  You are allowed to add your code to the bodies of test routines.
--  Such changes will be kept during further regeneration of this file.
--  All code placed outside of test routine bodies will be lost. The
--  code intended to set up and tear down the test environment should be
--  placed into Tile.Test_Data.

with AUnit.Assertions; use AUnit.Assertions;

package body Tile.Test_Data.Tests is


--  begin read only
   procedure Test_Init (Gnattest_T : in out Test);
   procedure Test_Init_f90d87 (Gnattest_T : in out Test) renames Test_Init;
--  id:2.2/f90d87d557b77ab6/Init/1/0/
   procedure Test_Init (Gnattest_T : in out Test) is
   --  tile.ads:7:3:Init
--  end read only

      T : Tile_Type;
      B : Boolean;
   begin
      T := Init(False);
      AUnit.Assertions.Assert(T.Value = 0 and T.Merged = False, "Test 1/3 : Tile Value must be set to 0");

      T := Init(True);
      AUnit.Assertions.Assert((T.Value = 2 or T.Value = 4) and T.Merged = False, "Test 2/3 : Tile Value must be set to 2 or 4");

      B := False;
      for I in 0 .. 100 loop
         T := Init(True);
         if (T.Value = 4) then
            B := True;
            exit;
         end if;
      end loop;

      AUnit.Assertions.Assert(B, "Test 3/3 : Tile Value must be set to 4 sometimes");

--  begin read only
   end Test_Init;
--  end read only


--  begin read only
   procedure Test_Get_Value (Gnattest_T : in out Test);
   procedure Test_Get_Value_a2ee8d (Gnattest_T : in out Test) renames Test_Get_Value;
--  id:2.2/a2ee8d7dfa276765/Get_Value/1/0/
   procedure Test_Get_Value (Gnattest_T : in out Test) is
   --  tile.ads:12:3:Get_Value
--  end read only

   T : Tile_Type;

   begin

   T := Init(False);

   AUnit.Assertions.Assert(Get_Value(T) = 0, "Test 1/2 : Tile Value must be set to 0");

   T := Init(True);

   AUnit.Assertions.Assert(Get_Value(T) = 2 or Get_Value(T) = 4, "Test 2/2 : Tile Value must be set to 2 or 4");

--  begin read only
   end Test_Get_Value;
--  end read only


--  begin read only
   procedure Test_Set_Value (Gnattest_T : in out Test);
   procedure Test_Set_Value_a1cba6 (Gnattest_T : in out Test) renames Test_Set_Value;
--  id:2.2/a1cba63cee00f9c3/Set_Value/1/0/
   procedure Test_Set_Value (Gnattest_T : in out Test) is
   --  tile.ads:14:3:Set_Value
--  end read only

      T : Tile_Type;

   begin

      Set_Value(T, 2);

      AUnit.Assertions.Assert(Get_Value(T) = 2, "Test 1/1 : Set the value of a tile at 2");

--  begin read only
   end Test_Set_Value;
--  end read only


--  begin read only
   procedure Test_Move_Tile (Gnattest_T : in out Test);
   procedure Test_Move_Tile_1cee4a (Gnattest_T : in out Test) renames Test_Move_Tile;
--  id:2.2/1cee4a7a936b0c85/Move_Tile/1/0/
   procedure Test_Move_Tile (Gnattest_T : in out Test) is
   --  tile.ads:17:3:Move_Tile
--  end read only

      T1 : Tile_Type;
      T2 : Tile_Type;

   begin
      Set_Value(T1, 8);
      Move_Tile(T1, T2);
      
      AUnit.Assertions.Assert(Get_Value(T1) = 0 and Get_Value(T2) = 8, "Test 1/2 : Move 1 tile to an other");

      Move_Tile(T2, T1);
      
      AUnit.Assertions.Assert(Get_Value(T1) = 8 and Get_Value(T2) = 0, "Test 2/2 : Move 1 tile to an other");

--  begin read only
   end Test_Move_Tile;
--  end read only


--  begin read only
   procedure Test_Merge_Tiles (Gnattest_T : in out Test);
   procedure Test_Merge_Tiles_61af64 (Gnattest_T : in out Test) renames Test_Merge_Tiles;
--  id:2.2/61af64aa5e2c2d6c/Merge_Tiles/1/0/
   procedure Test_Merge_Tiles (Gnattest_T : in out Test) is
   --  tile.ads:23:3:Merge_Tiles
--  end read only

      T1 : Tile_Type;
      T2 : Tile_Type;

   begin
      Set_Value(T1, 8);
      Set_Value(T2, 8);
      Merge_Tiles(T1, T2);
      
      AUnit.Assertions.Assert(Get_Value(T1) = 0 and Get_Value(T2) = 16, "Test 1/2 : Merge 2 tiles");
      
      Set_Merged(T2, False);
      Set_Value(T1, 16);
      Merge_Tiles(T2, T1);
      
      AUnit.Assertions.Assert(Get_Value(T1) = 32 and Get_Value(T2) = 0, "Test 2/2 : Merge 2 tiles");

--  begin read only
   end Test_Merge_Tiles;
--  end read only


--  begin read only
   procedure Test_Is_Merged (Gnattest_T : in out Test);
   procedure Test_Is_Merged_984185 (Gnattest_T : in out Test) renames Test_Is_Merged;
--  id:2.2/98418514c55c6993/Is_Merged/1/0/
   procedure Test_Is_Merged (Gnattest_T : in out Test) is
   --  tile.ads:33:3:Is_Merged
--  end read only

   T : Tile_Type;

   begin

      T := Init(False);

      AUnit.Assertions.Assert(not T.Merged, "Test 1/2 : Tile Merged value must be set to False at the initialization");

      T.Merged := True;

      AUnit.Assertions.Assert(T.Merged, "Test 2/2 : Tile Merged value must be set to True");




--  begin read only
   end Test_Is_Merged;
--  end read only


--  begin read only
   procedure Test_Set_Merged (Gnattest_T : in out Test);
   procedure Test_Set_Merged_341ca5 (Gnattest_T : in out Test) renames Test_Set_Merged;
--  id:2.2/341ca5f0b696f405/Set_Merged/1/0/
   procedure Test_Set_Merged (Gnattest_T : in out Test) is
   --  tile.ads:35:3:Set_Merged
--  end read only

   T : Tile_Type;

   begin

      T := Init(False);
      T.Merged := True;
      Set_Merged(T, False);

      AUnit.Assertions.Assert(not T.Merged, "Test 1/2 : Tile Merged value must be set to False");

      Set_Merged(T, True);

      AUnit.Assertions.Assert(T.Merged, "Test 2/2 : Tile Merged value must be set to True");

--  begin read only
   end Test_Set_Merged;
--  end read only


--  begin read only
   procedure Test_Inc_Value (Gnattest_T : in out Test);
   procedure Test_Inc_Value_682eb4 (Gnattest_T : in out Test) renames Test_Inc_Value;
--  id:2.2/682eb419c0422a90/Inc_Value/1/0/
   procedure Test_Inc_Value (Gnattest_T : in out Test) is
   --  tile.ads:40:3:Inc_Value
--  end read only

      T : Tile_Type;

   begin
      Set_Value(T, 2);
      Inc_Value(T);

      AUnit.Assertions.Assert(Get_Value(T) = 4, "Test 1/2 : Increment a 2 tile gives a 4 tile");

      Inc_Value(T);

      AUnit.Assertions.Assert(Get_Value(T) = 8, "Test 2/2 : Increment a 4 tile gives a 8 tile");

--  begin read only
   end Test_Inc_Value;
--  end read only


--  begin read only
   procedure Test_Reset_Value (Gnattest_T : in out Test);
   procedure Test_Reset_Value_ba60aa (Gnattest_T : in out Test) renames Test_Reset_Value;
--  id:2.2/ba60aa11b727eb02/Reset_Value/1/0/
   procedure Test_Reset_Value (Gnattest_T : in out Test) is
   --  tile.ads:44:3:Reset_Value
--  end read only

     T : Tile_Type;

   begin
      Set_Value(T, 2);
      Inc_Value(T);
      Reset_Value(T);

      AUnit.Assertions.Assert(Get_Value(T) = 0, "Test 1/2 : Reset a tile must set the value to 0");

--  begin read only
   end Test_Reset_Value;
--  end read only

end Tile.Test_Data.Tests;
