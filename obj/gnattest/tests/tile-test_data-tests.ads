--  This package has been generated automatically by GNATtest.
--  Do not edit any part of it, see GNATtest documentation for more details.

--  begin read only
with Gnattest_Generated;

package Tile.Test_Data.Tests is

   type Test is new GNATtest_Generated.GNATtest_Standard.Tile.Test_Data.Test
   with null record;

   procedure Test_Init_f90d87 (Gnattest_T : in out Test);
   --  tile.ads:7:3:Init

   procedure Test_Get_Value_a2ee8d (Gnattest_T : in out Test);
   --  tile.ads:12:3:Get_Value

   procedure Test_Set_Value_a1cba6 (Gnattest_T : in out Test);
   --  tile.ads:14:3:Set_Value

   procedure Test_Move_Tile_1cee4a (Gnattest_T : in out Test);
   --  tile.ads:17:3:Move_Tile

   procedure Test_Merge_Tiles_61af64 (Gnattest_T : in out Test);
   --  tile.ads:23:3:Merge_Tiles

   procedure Test_Is_Merged_984185 (Gnattest_T : in out Test);
   --  tile.ads:33:3:Is_Merged

   procedure Test_Set_Merged_341ca5 (Gnattest_T : in out Test);
   --  tile.ads:35:3:Set_Merged

   procedure Test_Inc_Value_682eb4 (Gnattest_T : in out Test);
   --  tile.ads:40:3:Inc_Value

   procedure Test_Reset_Value_ba60aa (Gnattest_T : in out Test);
   --  tile.ads:44:3:Reset_Value

end Tile.Test_Data.Tests;
--  end read only
