--  This package has been generated automatically by GNATtest.
--  You are allowed to add your code to the bodies of test routines.
--  Such changes will be kept during further regeneration of this file.
--  All code placed outside of test routine bodies will be lost. The
--  code intended to set up and tear down the test environment should be
--  placed into Game_Grid.Test_Data.

with AUnit.Assertions; use AUnit.Assertions;

package body Game_Grid.Test_Data.Tests is


--  begin read only
   procedure Test_Init (Gnattest_T : in out Test);
   procedure Test_Init_d7f813 (Gnattest_T : in out Test) renames Test_Init;
--  id:2.2/d7f8131ef239d4f2/Init/1/0/
   procedure Test_Init (Gnattest_T : in out Test) is
   --  game_grid.ads:14:3:Init
--  end read only

      M : Matrix;
      Count : Integer;
   begin
        M := Init;
        Count := 0;
        for Y in Height_Range loop
          for X in Width_Range loop
            if (Get_Value(M(Y, X)) = 2 or Get_Value(M(Y, X)) = 4) then
              Count := Count + 1;
            end if;
          end loop;
        end loop;

      AUnit.Assertions.Assert
        (Count = 1,
         "Matrix not correctly initialized with only 1 tile set to 2 or 4.");

--  begin read only
   end Test_Init;
--  end read only


--  begin read only
   procedure Test_Update (Gnattest_T : in out Test);
   procedure Test_Update_4147be (Gnattest_T : in out Test) renames Test_Update;
--  id:2.2/4147be197a268051/Update/1/0/
   procedure Test_Update (Gnattest_T : in out Test) is
   --  game_grid.ads:15:3:Update
--  end read only

       M : Matrix;

   begin


        for Y in Height_Range loop
          for X in Width_Range loop
            Set_Value(M(Y, X), 0);
          end loop;
        end loop;

      Set_Value(M(0, 0), 2);

      Update(M, None);

      AUnit.Assertions.Assert(Get_Value(M(0, 0)) = 2,
         "Test 1/8 : None move failed");

      Update(M, Right);

      AUnit.Assertions.Assert(Get_Value(M(0, 3)) = 2,
         "Test 2/8 : Right move without merge and without any other tile in the direction failed");


        for Y in Height_Range loop
          for X in Width_Range loop
            Set_Value(M(Y, X), 0);
          end loop;
        end loop;

      Set_Value(M(0, 0), 4);
      Set_Value(M(0, 3), 2);

      Update(M, Right);

      AUnit.Assertions.Assert(Get_Value(M(0, 2)) = 4 and Get_Value(M(0, 3)) = 2,
         "Test 3/8 : Right move without merge and with an other tile in the direction failed");

      for Y in Height_Range loop
          for X in Width_Range loop
            Set_Value(M(Y, X), 0);
          end loop;
        end loop;

      Set_Value(M(0, 2), 4);
      Set_Value(M(0, 3), 2);
      Set_Value(M(0, 0), 4);

      Update(M, Right);

      AUnit.Assertions.Assert(Get_Value(M(0, 2)) = 8 and Get_Value(M(0, 3)) = 2,
         "Test 4/8 : Right move with merge and with an other tile in the direction failed");


      for Y in Height_Range loop
          for X in Width_Range loop
            Set_Value(M(Y, X), 0);
          end loop;
        end loop;

      Set_Value(M(0, 2), 8);
      Set_Value(M(0, 3), 2);
      Set_Value(M(0, 0), 2);

      Update(M, Left);

      AUnit.Assertions.Assert(Get_Value(M(0, 0)) = 2
                                                  and Get_Value(M(0, 1)) = 8
                                                  and Get_Value(M(0, 2)) = 2,
         "Test 5/8 : Left move without merge and with an other tile in the direction failed");

      for Y in Height_Range loop
          for X in Width_Range loop
            Set_Value(M(Y, X), 0);
          end loop;
        end loop;

      Set_Value(M(0, 2), 2);
      Set_Value(M(0, 1), 8);
      Set_Value(M(0, 0), 2);
      Set_Value(M(0, 3), 4);

      Update(M, Down);

      AUnit.Assertions.Assert(Get_Value(M(3, 0)) = 2
                                                  and Get_Value(M(3, 1)) = 8
                                                  and Get_Value(M(3, 2)) = 2
                                                  and Get_Value(M(3, 3)) = 4,
         "Test 6/8 : Down move without merge and without any other tile in the direction failed");

      for Y in Height_Range loop
          for X in Width_Range loop
            Set_Value(M(Y, X), 0);
          end loop;
        end loop;

      Set_Value(M(3, 2), 2);
      Set_Value(M(3, 1), 8);
      Set_Value(M(3, 0), 2);
      Set_Value(M(3, 3), 4);
      Set_Value(M(0, 0), 2);

      Update(M, Down);

      AUnit.Assertions.Assert(Get_Value(M(3, 0)) = 4
                                                  and Get_Value(M(3, 1)) = 8
                                                  and Get_Value(M(3, 2)) = 2
                                                  and Get_Value(M(3, 3)) = 4,
         "Test 7/8 : Down move with merge and with an other tile in the direction failed");

      for Y in Height_Range loop
          for X in Width_Range loop
            Set_Value(M(Y, X), 0);
          end loop;
        end loop;

      Set_Value(M(3, 2), 2);
      Set_Value(M(3, 1), 8);
      Set_Value(M(3, 0), 4);
      Set_Value(M(3, 3), 4);
      Set_Value(M(0, 0), 4);

      Update(M, Up);

      AUnit.Assertions.Assert(Get_Value(M(0, 0)) = 8
                                                  and Get_Value(M(0, 1)) = 8
                                                  and Get_Value(M(0, 2)) = 2
                                                  and Get_Value(M(0, 3)) = 4,
         "Test 8/8 : Up move with merge and without any other tile in the direction failed");

      

--  begin read only
   end Test_Update;
--  end read only


--  begin read only
   procedure Test_is_Game_Over (Gnattest_T : in out Test);
   procedure Test_is_Game_Over_f5e1be (Gnattest_T : in out Test) renames Test_is_Game_Over;
--  id:2.2/f5e1beb8d35f5dcf/is_Game_Over/1/0/
   procedure Test_is_Game_Over (Gnattest_T : in out Test) is
   --  game_grid.ads:16:3:is_Game_Over
--  end read only

      M : Matrix;

   begin
      M := Init;

      AUnit.Assertions.Assert(is_Game_Over(M) = False, "Test 1/5 : The game should not be over after initialisation");

      for Y in Height_Range loop
        for X in Width_Range loop
          if X mod 2 = 0 then
            if Y mod 2 = 0 then
              Set_Value(M(Y, X), 2);
            else
              Set_Value(M(Y, X), 4);
            end if;
          else
            if Y mod 2 = 0 then
              Set_Value(M(Y, X), 4);
            else
              Set_Value(M(Y, X), 2);
            end if;
          end if;
        end loop;
      end loop;

      Set_Value(M(0, 0), 4);

      AUnit.Assertions.Assert(is_Game_Over(M) = False, "Test 2/5 : The game should not be over with 2 equal values side by side in the Matrix");

      Set_Value(M(0, 0), 0);

      AUnit.Assertions.Assert(is_Game_Over(M) = False, "Test 3/5 : The game should not be over with a 0 in the Matrix");

      Set_Value(M(0, 0), 2);

      AUnit.Assertions.Assert(is_Game_Over(M) = True, "Test 4/5 : The game should be over (no movement can be done)");

      M := Init;

      Set_Value(M(0,0), 2048);

      AUnit.Assertions.Assert(is_Game_Over(M) = True, "Test 5/5 : The game should be over (There is a 2048 tile)");

--  begin read only
   end Test_is_Game_Over;
--  end read only

end Game_Grid.Test_Data.Tests;
