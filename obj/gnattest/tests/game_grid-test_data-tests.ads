--  This package has been generated automatically by GNATtest.
--  Do not edit any part of it, see GNATtest documentation for more details.

--  begin read only
with Gnattest_Generated;

package Game_Grid.Test_Data.Tests is

   type Test is new GNATtest_Generated.GNATtest_Standard.Game_Grid.Test_Data.Test
   with null record;

   procedure Test_Init_d7f813 (Gnattest_T : in out Test);
   --  game_grid.ads:14:3:Init

   procedure Test_Update_4147be (Gnattest_T : in out Test);
   --  game_grid.ads:15:3:Update

   procedure Test_is_Game_Over_f5e1be (Gnattest_T : in out Test);
   --  game_grid.ads:16:3:is_Game_Over

end Game_Grid.Test_Data.Tests;
--  end read only
