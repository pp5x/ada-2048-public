--  This package has been generated automatically by GNATtest.
--  Do not edit any part of it, see GNATtest documentation for more details.

--  begin read only
with Gnattest_Generated;

package Random.Test_Data.Tests is

   type Test is new GNATtest_Generated.GNATtest_Standard.Random.Test_Data.Test
   with null record;

   procedure Test_Init_aa3802 (Gnattest_T : in out Test);
   --  random.ads:2:3:Init

   procedure Test_Get_Next_54fdfb (Gnattest_T : in out Test);
   --  random.ads:3:3:Get_Next

end Random.Test_Data.Tests;
--  end read only
