--  This package has been generated automatically by GNATtest.
--  You are allowed to add your code to the bodies of test routines.
--  Such changes will be kept during further regeneration of this file.
--  All code placed outside of test routine bodies will be lost. The
--  code intended to set up and tear down the test environment should be
--  placed into Random.Test_Data.

with AUnit.Assertions; use AUnit.Assertions;

package body Random.Test_Data.Tests is


--  begin read only
   procedure Test_Init (Gnattest_T : in out Test);
   procedure Test_Init_aa3802 (Gnattest_T : in out Test) renames Test_Init;
--  id:2.2/aa38024ab3eb42c9/Init/1/0/
   procedure Test_Init (Gnattest_T : in out Test) is
   --  random.ads:2:3:Init
--  end read only

      pragma Unreferenced (Gnattest_T);

   begin
      null;

--  begin read only
   end Test_Init;
--  end read only


--  begin read only
   procedure Test_Get_Next (Gnattest_T : in out Test);
   procedure Test_Get_Next_54fdfb (Gnattest_T : in out Test) renames Test_Get_Next;
--  id:2.2/54fdfb6a3f119b3d/Get_Next/1/0/
   procedure Test_Get_Next (Gnattest_T : in out Test) is
   --  random.ads:3:3:Get_Next
--  end read only

     Inc : Integer;

   begin
     Init(150);
     Inc := 0;
     for I in 0 .. 10000 loop
       Inc := Inc + Integer(get_Next mod 100);
     end loop;
     Inc := Inc / 10001;

     AUnit.Assertions.Assert(Inc >= 49 and Inc <= 51,
     "Test 1/2 : 10001 calls o Random should cover all number between 0 and 100");

     Init(25);
     Inc := 0;
     for I in 0 .. 10000 loop
       Inc := Inc + Integer(get_Next mod 100);
     end loop;
     Inc := Inc / 10001;

     AUnit.Assertions.Assert(Inc >= 49 and Inc <= 51,
     "Test 2/2 : 10001 calls o Random should cover all number between 0 and 100");

--  begin read only
   end Test_Get_Next;
--  end read only

end Random.Test_Data.Tests;
