# Processus de développement

## Exigence système

Programme réalisant un jeu basé sur le puzzle 2048.

Contient une grille où il y a des tuiles sur lesquelles on effectue des
mouvements.

## Méthodologie de tracabilité

### Convention de nommage des exigences de haut niveau

Les exigences de haut niveau seront identifiées selon le codage suivant :

* Prefixe `HLR` pour indiquer *haut niveau* : *High Level Requirement*
* Numéro de l'exigence en base 10.

#### Exemples

* `HLR10`
* `HLR42`

### Convention de nommage des exigences de bas niveau

Les exigences de bas niveau seront identifiées selon le codage suivant :

* Prefixe `LLR` pour indiquer *bas niveau* : *Low Level Requirement*
* Un tiret `-`
* Référence de l'exigence de haut niveau
* Un tiret `-`
* Numéro de l'exigence en base 10.

#### Exemples

* `LLR-HLR10-1`
* `LLR-HLR42-1`

### Convention de nommage des points d'architecture

Les points d'architecture du logiciel sont référencés selon le codage suivant :

* Préfixe `A`
* Numéro du point d'architecture en base 10.

#### Exemples

* `A12`
* `A32`

## Exigences de haut niveau

--------------------------------------------------------------------------------
**Référence**   **Description**
--------------- ----------------------------------------------------------------
`HLR1`          Création d'une grille de tuile de 4x4. Les tuiles sont générées
                aléatoirement par le générateur de nombres aléatoires `HLR3`.

`HLR2`          Les tuiles contiennent des numéros en puissance de 2. Le rang
                des valeurs possibles est entre 2 et 2048.

`HLR3`          Générateur de nombres aléatoires : le caractère aléatoire n'est
                pas critique, il doit respecter une répartion à peu près
                uniforme d'au moins 20% sur 100 tirages. Il doit pouvoir
                générer des nombres entres 0 et 100.

`HLR4`          Une entrée utilisateur est une direction donnée par : *gauche*,
                *droite*, *haut* et *bas*. En fonction de la direction choisie,
                la grille doit se mettre à jour en déplacant les tuiles dans la
                direction souhaitée. Les tuiles ne doivent pas sortir de la
                grille.

`HLR5`          Lors du déplacement des tuiles dans une direction (`HLR4`) : 2
                tuiles adjacentes par rapport à la direction doivent fusionner
                que si leur valeur est identique. Après il doit rester après
                une fusion, une seule et unique tuile localisée le plus loin
                que possible dans le sens du mouvement. Deux tuiles fusionnées
                ne peuvent pas, lors d'un mouvement, fusionner avec une autre
                tuile.

`HLR6`          Lors du déplacement des tuiles dans une direction (`HLR4`) : 2
                tuiles adjacentes par rapport à la direction ne doivent pas
                fusionner si leur valeur (`HLR2`) sont différentes. Elles se
                déplacent dans la direction aussi loin que possible.

`HLR7`          Le jeu doit s'arrêter lorsque le joueur a perdu ou gagne. Un
                gameover est déclaré lorsque plus aucun mouvement de tuile
                n'est possible sur la grille. La partie est gagnée lorsque la
                valeur 2048 est présente dans la grille.

`HLR8`          Le joueur est notifie lorsqu'il gagne ou perd la partie. Il
                peut recommencer une partie en appuyant sur le bouton *reset*
                de la carte.

-------------------------------------------------------------------------------

![Format de la grille (`HLR1`)](ui_grid)

![Mouvement des tuiles (`HLR5` et `HLR6`)](farthest_cell)

## Architecture du logiciel

--------------------------------------------------------------------------------
**Référence**   **Description**
--------------- ----------------------------------------------------------------
`A1`            Générateur de nombre aléatoire (Hardware ou Software).

`A2`            Gestion d'une tuile.

`A3`            Gestion de la grille.

--------------------------------------------------------------------------------

## Exigences de bas niveau

--------------------------------------------------------------------------------
**Référence**   **Description**
--------------- ----------------------------------------------------------------
`LLR-HLR1-1`    Présence d'un type traduisant les dimensions de la grille
                (`A3`) (hauteur et largeur). Le type est créé par rapport à 2
                constantes : la hauteur et la largeur de la grille.

`LLR-HLR1-2`    Définition du type grille (`Matrix`) représentant un tableau à 2
                dimensions. Les dimensions sont définies en utilisant les types
                hauteur et largeur.

`LLR-HLR1-3`    Fonction de remplissage aléatoire de la grille (utilisation de
                `A1`). Tirage aléatoire d'un nombre entre 0 et 15 (position de
                la grille) et tirage aléatoire d'un nombre entre 0 et 100 pour
                déterminer la valeur initiale de la tuile (2 si inférieur à 70;
                4 sinon).

`LLR-HLR2-1`    Présence d'un type tuile contenant un champ avec la valeur
                courante de la tuile. La valeur minimale de la tuile est 2 et
                évolue par multiplication par 2 de la valeur courante jusqu'à
                atteindre la valeur 2048.

`LLR-HLR3-1`    Présence d'une fonction prenant en paramètre la valeur maximale
                du tirage. La valeur minimale est fixée à 0. Le nombre est
                généré soit par un algorithme pseudo-aléatoire ou par un
                périphérique hardware.

`LLR-HLR4-1`    Présence d'une énumération indiquant 4 états : gauche, droite,
                haut et bas.

`LLR-HLR4-2`    Algorithme de manipulation de la grille (`A3`) pour effectuer
                les déplacements des tuiles (`A2`).
--------------------------------------------------------------------------------

\newpage

--------------------------------------------------------------------------------
**Référence**   **Description**
--------------- ----------------------------------------------------------------
`LLR-HLR5-1`    Algorithme de détection de fusion lors d'un déplacement de
                tuiles.

`LLR-HLR5-2`    Présence d'une fonction prennant les deux tuiles à fusionner,
                ainsi que la direction du déplacement. La tuile fusionnée est
                multipliée par deux, son état passe à "fusionnée" et la
                seconde tuile est mise à un état neutre (sans valeur, une
                tuile vide).

`LLR-HLR6-1`    L'algorithme de détection de fusion doit, si aucune fusion
                n'est nécessaire, ne rien faire.

`LLR-HLR7-1`    Un algorithme vérifie qu'il n'existe aucune tuile neutre
                (ayant comme valeur 0).

`LLR-HLR7-2`    Un algorithme verifie, si `LLR-HLR7-1` ne trouve pas de tuile
                neutre, qu'il existe au moins deux tuiles ayant la même valeur
                côte à côte (Dans les deux dimensions de la grille).

`LLR-HLR7-3`    Si `LLR-HLR7-1` et `LLR-HLR7-2` retournent un état faux, aucun
                mouvement n'est possible, le joueur a perdu, la boucle de jeu
                doit s'arrêter. Le programme doit comporter une variable
                booléenne intégrée à la boucle de jeu qui indique si la
                partie est terminée (perdue ou gagnée).

`LLR-HLR7-4`    Si une tuile présente dans le jeu porte le numéro 2048, la
                partie est terminée et le joueur gagne.

`LLR-HLR8-1`    Le joueur est averti qu'il a perdu si `LLR-HLR7-3` est
                vérifiée.

`LLR-HRL8-2`    Le joueur est avertit qu'il a gagné si `LLR-HLR7-4` est
                vérifiée.
--------------------------------------------------------------------------------

\newpage

# Processus de vérification

## Méthodologie de tracabilité

Les exigences de bas-niveau sont tracées au code source suivant cette
méthodologie :

* Commentaire dans le code qui comporte la référence de l'exigence de
bas-niveau suivie par une brève description.

#### Exemples

* `-- LLR-HLR1-1: Defines program global state`
* `-- LLR-HLR2-1: Pathfinding algorithm`

## Procédures de vérification

La procédure de vérification ne prend pas en compte l'aspect graphique du
projet, elle permet de vérifier l'ensemble des éléments de l'architecture
présentée dans ce présent rapport.

Les tests s'organisent donc de la manière suivante :

* Tests du module `Random`

* Tests du module `Game_Grid`

* Tests du module `Tile`

Pour la suite de cette partie, les tests respecterons la nomenclature
suivante :

`Test-{Module}-{Série}-{numéro du test}`

L'ensemble des tests unitaires on été créés en utilisant l'outil `gnattest`.

### Tests - Module `Random`

Le module comprend 1 test, il permet de voir si la répartition de l'aléatoire
est presque uniforme.

Le test porte donc sur la fonction `Get_Next`.

--------------------------------------------------------------------------------
**Référence**         **Description**                          **But**
--------------------- ---------------------------------------- -----------------
`Test-Random-1-1`       - Appel de la fonction                  Vérifier la
                        d'initialisation du Random\newline      `LLR-HLR3-1`
                        - Faire 10001 appels de la fonction
                        `Get_Next` modulo 100\newline
                        - Vérifier que la moyenne est proche
                        de 50

--------------------------------------------------------------------------------

### Tests - Module `Game_Grid`

Le module comprend 3 series de tests, chaque série associée à une
fonction ou procédure du module.

Celui-ci est composé de :

* La fonction Init qui initialise la matrice de jeu

* La fonction Update qui met à jour la matrice de jeu en effectant un
mouvement

* La fonction Is_Game_Over qui vérifie que le jeu n'est pas dans un 
état bloqué

--------------------------------------------------------------------------------
**Référence**         **Description**                          **But**
--------------------- ---------------------------------------- -----------------
`Test-Game_Grid-1-1`    - Appel de la fonction                  Remplissage
                        d'initialisation de la grille\newline   correct de la
                        - Compter combien de tuile ont été      grille
                        initialisées à 2 ou à 4

`Test-Game_Grid-2-1`    - Mise à zéro de la grille\newline      La grille reste
                        - Valeur de la tuile (0, 0) à 2\newline inchangée
                        - Appel de `Update` avec la direction
                        `None`

`Test-Game_Grid-2-2`    - Mise à zéro de la grille\newline      La grille se
                        - Valeur de la tuile (0, 0) à 2\newline met à jour en
                        - Appel de `Update` avec la direction     respectant la
                        `Droite`                                `LLR-HLR4-2`

`Test-Game_Grid-2-3`    - Mise à zéro de la grille\newline      La grille se
                        - Valeur de la tuile (0, 0) à 4\newline met à jour en
                        - Valeur de la tuile (0, 3) à 2\newline respectant la
                        - Appel de `Update` avec la direction     `LLR-HLR4-2`
                        `Droite` 

`Test-Game_Grid-2-4`    - Mise à zéro de la grille\newline      La grille se
                        - Valeur de la tuile (0, 0) à 4\newline met à jour en
                        - Valeur de la tuile (0, 3) à 2\newline respectant la
                        - Valeur de la tuile (0, 2) à 4\newline `LLR-HLR4-2` et
                        - Appel de `Update` avec la direction     `LLR-HLR5-2`
                        `Droite`

`Test-Game_Grid-2-5`    - Mise à zéro de la grille\newline      La grille se
                        - Valeur de la tuile (0, 0) à 2\newline met à jour en
                        - Valeur de la tuile (0, 3) à 2\newline respectant la
                        - Valeur de la tuile (0, 2) à 8\newline `LLR-HLR4-2`
                        - Appel de `Update` avec la direction
                        `Gauche`

`Test-Game_Grid-2-6`    - Mise à zéro de la grille\newline      La grille se
                        - Valeur de la tuile (0, 2) à 2\newline met à jour en
                        - Valeur de la tuile (0, 1) à 8\newline respectant la
                        - Valeur de la tuile (0, 0) à 2\newline `LLR-HLR4-2`
                        - Valeur de la tuile (0, 3) à 4\newline
                        - Appel de `Update` avec la direction
                        `Bas`

`Test-Game_Grid-2-7`    - Mise à zéro de la grille\newline      La grille se
                        - Valeur de la tuile (3, 2) à 2\newline met à jour en
                        - Valeur de la tuile (3, 1) à 8\newline respectant la
                        - Valeur de la tuile (3, 0) à 2\newline `LLR-HLR4-2` et
                        - Valeur de la tuile (3, 3) à 4\newline `LLR-HLR5-2`
                        - Valeur de la tuile (0, 0) à 2\newline
                        - Appel de `Update` avec la direction
                        `Bas`

`Test-Game_Grid-2-8`    - Mise à zéro de la grille\newline      La grille se
                        - Valeur de la tuile (3, 2) à 2\newline met à jour en
                        - Valeur de la tuile (3, 1) à 8\newline respectant la
                        - Valeur de la tuile (3, 0) à 4\newline `LLR-HLR4-2` et
                        - Valeur de la tuile (3, 3) à 4\newline `LLR-HLR5-2`
                        - Valeur de la tuile (0, 0) à 4\newline
                        - Appel de `Update` avec la direction
                        `Haut`

`Test-Game_Grid-3-1`    - Initialisation de la matrice\newline  Vérifier la
                        - Appel de `Is_Game_Over`                 `LLR-HLR7-1`

`Test-Game_Grid-3-2`    - Mise de la grille à l'état            Vérifier la
                        bloqué\newline                          `LLR-HLR7-2`
                        - Mise à la même valeur de 2 tuiles
                        côte à côte\newline
                        - Appel de `is_Game_Over`

`Test-Game_Grid-3-3`    - Mise de la grille à l'état            Vérifier la
                        bloqué\newline                          `LLR-HLR7-1`
                        - Mise à zéro d'une tuile
                        - Appel de `is_Game_Over`

`Test-Game_Grid-3-4`    - Mise de la grille à l'état            Vérifier la
                        bloqué\newline                          `LLR-HLR7-3`
                        - Appel de `is_Game_Over`

`Test-Game_Grid-3-5`    - Initialisation de la grille\newline   Vérifier la
                        - Mise à 2048 d'une tuile                `LLR-HLR7-4`

--------------------------------------------------------------------------------


### Tests - Module `Tile`

Le module comprend 9 series de tests, chaque série associée à une
fonction ou procédure du module.

Celui-ci est composé de :

* `Init`
* `Get_Value`
* `Set_Value`
* `Move_Tile`
* `Merge_Tiles`
* `Is_Merged`
* `Set_Merged`
* `Inc_Value`
* `Reset_Value`

--------------------------------------------------------------------------------
**Référence**         **Description**                          **But**
--------------------- ---------------------------------------- -----------------
`Test-Tile-1-1`         - Appel de la fonction                  Création d'une
                        d'initialisation de la Tuile avec       tuile neutre
                        comme paramètre `Faux`

`Test-Tile-1-2`         - Appel de la fonction                  Création d'une
                        d'initialisation de la Tuile avec       tuile initiale
                        comme paramètre `Vrai`

`Test-Tile-1-3`         - Appel de la fonction                  Création d'une
                        d'initialisation de la Tuile avec       tuile ayant
                        comme paramètre `Vrai` 100 fois         comme valeur 4

`Test-Tile-2-1`         - Appel de la fonction                  Valeur de la
                        d'initialisation de la Tuile avec       tuile égale à 0
                        comme paramètre `Faux`\newline
                        - Appel de la fonction `Get_Value`

`Test-Tile-2-2`         - Appel de la fonction                  Valeur de la
                        d'initialisation de la Tuile avec       tuile égale à 2
                        comme paramètre `True`\newline              ou à 4
                        - Appel de la fonction `Get_Value`

`Test-Tile-3-1`         - Appel de la fonction                  Valeur de la
                        `Set_Value` avec comme paramètre 2        tuile mise à 2

`Test-Tile-4-1`         - Appel de la fonction                  Déplacement
                        `Move_Tile` entre deux tuiles, la tuile   d'une tuile
                        1 à pour valeur 8 et vas sur la tuile   `LLR-HLR4-2`
                        2 qui à pour valeur 0

`Test-Tile-5-1`         - Appel de la fonction                  Fusion de deux
                        `Merge_Tiles` entre deux tuiles, la       tuiles
                        tuile 1 à pour valeur 8 et vas sur      `LLR-HLR5-2`
                        la tuile 2 qui à pour valeur 8

`Test-Tile-6-1`         - Appel de la fonction                  Test de la
                        `Is_Merged` sur une tuile n'ayant pas     valeur Merged
                        sa valeur `Merged` mis à Faux             d'une tuile

`Test-Tile-6-2`         - Appel de la fonction                  Test de la
                        `Is_Merged` sur une tuile n'ayant pas     valeur Merged
                        sa valeur `Merged` mis à Vrai             d'une tuile

`Test-Tile-7-1`         - Appel de la fonction                  Changement de
                        `Set_Merged` sur une tuile pour mettre    la valeur
                        sa valeur `Merged` à Faux                 Merged d'une
                                                                tuile

`Test-Tile-7-2`         - Appel de la fonction                  Changement de
                        `Set_Merged` sur une tuile pour mettre    la valeur
                        sa valeur `Merged` à Vrai                 Merged d'une
                                                                tuile

`Test-Tile-8-1`         - Appel de la fonction                  Incrémentation
                        `Inc_Value` sur une tuile pour passer     de la valeur
                        sa valeur de 2 à 4                      d'une tuile

`Test-Tile-8-2`         - Appel de la fonction                  Incrémentation
                        `Inc_Value` sur une tuile pour passer     de la valeur
                        sa valeur de 4 à 8                      d'une tuile

`Test-Tile-9-1`         - Appel de la fonction                  Mise à zéro
                        `Reset_Value` sur une tuile pour mettre   de la valeur
                        sa valeur à 0                           d'une tuile

--------------------------------------------------------------------------------

## Résultat des tests

L'ensemble des tests ont été implémentés et 100% des tests sont vérifiés.

Les tests ont été implémentés dans le dossier `obj/gnattest/tests/`.

## Analyse de couverture

Grâce au programme `gnatcov`, il est possible d'étudier la couverture des
tests implémentés, nous avons donc utilisé ce programme afin d'analyser la
couverture de nos tests.

Tout d'abord, il faut lancer le programme avec notre suite de tests pour créer
un fichier de traces. Ensuite, il faut extraire la couverture à partir de ces
traces.

En étudiant les traces produites, nous constatons que la couverture est de 100%
dans tous les fichiers.
