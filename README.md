ADA-2048
========

Requirements
------------

GNAT community tools from AdaCore. Some tools have been removed because of copyright issues.

Bootstrap
---------

* `Vagrantfile`: Setup Ubuntu 14.04 64-bit with preinstalled tools.

### Vagrant

* `vagrant up`: launch the VM.
* `vagrant ssh`: connect to the VM via ssh.

After first launch: tarballs are already extracted in `/tmp/`. You must
compile/install each tool.

Do not forget to update your `$PATH` in `.bashrc`.

OpenOCD
-------

OpenOCD is already setup and can be used to upload an ELF file to the board with
the Makefile:

* `make download` upload the ELF to the board.
* `make debug` launch openocd and gdb.
* `make run` reset the board.

You may have to use `sudo` to access the device.

Project
-------

Use the sample project given by the teacher.

To succeed the upload on the board you have to compile the binary so it can be
uploaded to the ROM (exchange -XLOADER to ROM).

### How to play

Upload the game on the STM32F429 and use your fingers on the screen to move
numbers. If you achieve to reach 2048, the screen turns blue. If you failed, it
turns red.

### `Makefile`

The `Makefile` contains ready-to-use rules in order to compile, launch tests,
coverage and clean the repository.

The variable TARGET allows to select `pc` or `stm32` :

* `pc` allows you to compile the game on x86_64 in order to launch tests
without upload the binary to a board. It's very useful to do quick development.
* `stm32` will compile everything that is needed to make the game 2048 work on
the STM32F429-Discovery board.

### STM32 source code

The code that is compiled for STM32 is stored in these folders :

* `src/` : common code, the game logic (works on x86_64 and arm).
* `src/stm32/` : contains hardware specific code (using the given library) in
order to use the *screen interface*, the *hardware random generator*.

Tests
-----

You can run some unit-tests by running this command :

* `make tests`

And you can clean all tests compile files by running this command:

* `make clean-tests`
