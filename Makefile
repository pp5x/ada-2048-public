ELF		= main

OPENOCD		= openocd
OPENOCD_CFG	= openocd.cfg
OPENOCD_LOG	= openocd.log

NETCAT		= netcat

GDB		= arm-eabi-gdb
GDB_SCRIPTS	= gdb_scripts
GDB_INIT	= ${GDB_SCRIPTS}/gdb-init
GDB_DOWNLOAD	= ${GDB_SCRIPTS}/gdb-download

GPRBUILD	= gprbuild
GPRCLEAN	= gprclean
GPR_CFG		= game2048.gpr

OBJDIR		= obj

# stm32 / pc
TARGET		= stm32

${ELF}::
	${GPRBUILD} -P ${GPR_CFG} -XLOADER=ROM -XTARGET=${TARGET}

# Compile and download the application on the device.
download: ${ELF}
	arm-eabi-strip $<
	sudo ${OPENOCD} -f ${OPENOCD_CFG} -c 'program $< verify reset exit'

# Must be used with -XLOADER=RAM
download-ram: ${ELF}
	@sudo ${OPENOCD} -f ${OPENOCD_CFG} -l ${OPENOCD_LOG}&
	@${GDB} ${ELF} -x ${GDB_DOWNLOAD}

# Reset the device and run
run:
	@sudo ${OPENOCD} -f ${OPENOCD_CFG} -l ${OPENOCD_LOG}&
	@sleep 1 && echo 'reset run' | ${NETCAT} localhost 4444 > /dev/null
	@echo 'shutdown' | ${NETCAT} localhost 4444 > /dev/null

# Launch OpenOCD and GDB
debug:
	@sudo ${OPENOCD} -f ${OPENOCD_CFG} -l ${OPENOCD_LOG}&
	@${GDB} ${ELF} -x ${GDB_INIT}

clean:
	${GPRCLEAN} -P ${GPR_CFG} -XTARGET=${TARGET}
	${RM} -r ${OPENOCD_LOG} ${ELF}

# run tests
tests: TARGET := pc
tests: build-tests
	./obj/gnattest/harness/test_runner

build-tests:
	gnattest -P ${GPR_CFG} -XLOADER=ROM -XTARGET=${TARGET}
	gprbuild -P obj/gnattest/harness/test_driver.gpr -XTARGET=${TARGET} ${QUIET}

coverage: build-tests
	gnatcov64 run obj/gnattest/harness/test_runner
	gnatcov64 coverage --level=stmt --annotate=xcov test_runner.trace -P ${GPR_CFG} -XTARGET=${TARGET}

clean-tests: clean
	rm -r obj/gnattest/harness
	rm obj/*.xcov
	rm *.trace

# Makefile debug command
print-%: ; @echo $*=${$*}

.PHONY: download download-ram run debug clean tests build-tests coverage 
